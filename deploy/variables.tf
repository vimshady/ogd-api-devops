variable "prefix" {
  default = "ogd"
}

variable "project" {
  default = "ogd-api-devops"
}

variable "contact" {
  default = "eikheloa@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "ogd-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "740457230866.dkr.ecr.us-east-1.amazonaws.com/ogd-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "740457230866.dkr.ecr.us-east-1.amazonaws.com/ogd-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "ogd.app"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
