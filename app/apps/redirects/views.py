"""Views for the redirect app."""
from django.shortcuts import get_object_or_404
from django.views.generic.base import RedirectView
from django.http import HttpResponse

from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAdminUser
from dynamic_rest.viewsets import DynamicModelViewSet

from .models import Redirect
from .serializers import RedirectSerializer


class RedirectAPIViewSet(DynamicModelViewSet):
    """Handles requests for creating, retrieving, updating, and deleting redirects.

    Redirects can be linked to a unique QR codes by specifying a GET parameter
    (?with_qr=True). In the response, a redirect includes a QR image link in if
    (a) if specified in request (?include[]=qr.*) and (b) an instance exists.

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAdminUser,)

    queryset = Redirect.objects.all()
    serializer_class = RedirectSerializer

    def create(self, request, format=None):
        """Only POST requests accepted."""
        serializer = RedirectSerializer(data=request.data)
        if serializer.is_valid():
            redirect = serializer.save()
            # return qr image if ?qr provided as GET param
            if 'qr' in request.GET:
                return HttpResponse(
                    redirect.get_qr(),
                    content_type="image/png", status=status.HTTP_201_CREATED)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        queryset = Redirect.objects.all()
        redirect = get_object_or_404(queryset, pk=pk)
        serializer = RedirectSerializer(redirect)
        # return qr image if ?qr provided as GET param
        if 'qr' in request.GET:
            return HttpResponse(
                redirect.get_qr(),
                content_type="image/png", status=status.HTTP_200_OK)
        return Response(serializer.data)


class RedirectView(RedirectView):
    """Take visitor from redirect to target url."""

    permanent = False
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        redirect = get_object_or_404(Redirect, hash=kwargs['hash'])
        return redirect.get_default_target_url()
