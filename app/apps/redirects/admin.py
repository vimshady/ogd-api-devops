from django.contrib import admin
from .models import Redirect


class RedirectAdmin(admin.ModelAdmin):
    list_display = ['id', 'tag_list', 'target_url_list']

    def get_queryset(self, request):
        print('super().get_queryset(request)', super().get_queryset(request))
        for o in super().get_queryset(request).all():
            print(o.tags)
        return super().get_queryset(request)

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())


admin.site.register(Redirect, RedirectAdmin)
