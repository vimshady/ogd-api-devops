from dynamic_rest.serializers import DynamicModelSerializer
# DynamicRelationField
# from taggit_serializer.serializers import (
# TagListSerializerField, TaggitSerializer)

# from ogd.apps.qr.serializers import QRCodeSerializer
from .models import Redirect


class RedirectSerializer(DynamicModelSerializer):
    # removed mixin TaggitSerializer
    class Meta:
        model = Redirect
        name = 'redirect'
        fields = ('id', 'target_url_list', 'default_index')  # 'tags','qr'
        deferred_fields = ('qr', )
        read_only_fields = ('hash',)

    # tags = TagListSerializerField(required=False)
    # qr = DynamicRelationField(
        # QRCodeSerializer, read_only=True, deferred=True)

    def create(self, validated_data):
        """Create redirect method.

        If with_qr param passed, then also create a related qr code.

        """
        redirect = Redirect.objects.create(**validated_data)
        return redirect
