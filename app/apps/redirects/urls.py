from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import RedirectAPIViewSet


router = DefaultRouter()
router.register('redirects', RedirectAPIViewSet)

app_name = 'redirects'

urlpatterns = [
    path('', include(router.urls)),
]
