"""Data model related to redirect link."""

import os
from binascii import hexlify
import io

from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.utils.encoding import smart_text
from django.core.files.base import ContentFile

# from taggit.managers import TaggableManager
import segno


def create_redirect_hash():
    """Create 16 character hash for redirect link."""
    return smart_text(hexlify(os.urandom(8)))


class Redirect(models.Model):
    target_url_list = ArrayField(models.CharField(max_length=200))
    default_index = models.IntegerField(default=0)
    hash = models.CharField(
        max_length=20, default=create_redirect_hash, unique=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # tags = TaggableManager(blank=True)

    def __str__(self):
        """Return string."""
        return settings.REDIRECT_DOMAIN + 'r/' + self.hash

    def get_default_target_url(self):
        return self.target_url_list[self.default_index]

    def get_qr(self):
        out = io.BytesIO()
        qr = segno.make('hello world', micro=False)
        qr.save(out, kind='png', scale=8)
        return ContentFile(out.getvalue())
