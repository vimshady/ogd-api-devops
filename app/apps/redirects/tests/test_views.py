from django.test import TestCase, Client

from apps.redirects.models import Redirect


class RedirectTestCase(TestCase):

    def setUp(self):
        self.redirect = Redirect.objects.create(
            target_url_list=["http://google.com"])
        self.client = Client()

    def test_rediect(self):
        response = self.client.get('/r/' + self.redirect.hash + '/')
        self.assertRedirects(
            response, 'http://google.com', 302, fetch_redirect_response=False)
