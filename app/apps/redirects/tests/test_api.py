from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from apps.redirects.models import Redirect
from apps.redirects.serializers import RedirectSerializer


def detail_url(redirect_id):
    """Return redirect detail URL"""
    return reverse('redirects:redirect-detail', args=[redirect_id])


class RedirectAPITest(APITestCase):

    def setUp(self):
        self.url = "http://localhost:8000/api/v0/redirects/"
        self.redirect_post_data = {"target_url_list": ['https://google.com/']}
        self.client = APIClient()
        self.admin = get_user_model().objects.create(
            email='admin_user@test.com',
            password='changeme',
            is_staff=True
        )
        self.basic_user = get_user_model().objects.create(
            email='basic_user@test.com',
            password='changeme',
            is_staff=False
        )

    def test_authentication_required(self):
        """Test that authentication is required"""
        response = self.client.post(
            self.url, self.redirect_post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_redirect_with_api_as_staff(self):
        """Test that staff can create a redirect"""
        self.client.force_authenticate(user=self.admin)
        response = self.client.post(
            self.url, self.redirect_post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Redirect.objects.count(), 1)
        self.assertEqual(
            Redirect.objects.first().get_default_target_url(),
            'https://google.com/')

    def test_staff_permissions_required(self):
        """Test that staff_permissions is required"""
        self.client.force_authenticate(user=self.basic_user)
        response = self.client.post(
            self.url, self.redirect_post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_view_redirect_detail(self):
        """Test viewing a redirect detail"""
        redirect = Redirect.objects.create(
            target_url_list=['https://google.com/'])
        url = detail_url(redirect.id)
        self.client.force_authenticate(user=self.admin)
        response = self.client.get(url)
        serializer = RedirectSerializer(redirect)
        self.assertEqual(response.data, serializer.data)

    def test_partial_update_redirect(self):
        """Test updating a direct with patch"""
        redirect = Redirect.objects.create(
            target_url_list=['https://google.com/'])
        payload = {'target_url_list': ['https://espn.com/']}
        url = detail_url(redirect.id)
        self.client.force_authenticate(user=self.admin)
        self.client.patch(url, payload)

        redirect.refresh_from_db()
        self.assertEqual(redirect.target_url_list, payload['target_url_list'])

    def test_full_update_redirect(self):
        """Test updating a direct with put"""
        redirect = Redirect.objects.create(
            target_url_list=['https://google.com/'])
        payload = {"target_url_list": ['https://nytimes.com/']}
        url = detail_url(redirect.id)
        self.client.force_authenticate(user=self.admin)
        self.client.put(url, payload)

        redirect.refresh_from_db()
        self.assertEqual(redirect.target_url_list, payload['target_url_list'])

    def test_create_redirect_returning_qr(self):
        """Test creating a redirect and returning a qr image"""
        url_with_qr_param = self.url + '?qr'
        self.client.force_authenticate(user=self.admin)
        response = self.client.post(
            url_with_qr_param, self.redirect_post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Redirect.objects.count(), 1)
        self.assertEqual(
            response._headers['content-type'][1],
            'image/png'
        )

    def test_get_redirect_as_qr(self):
        """Test retrieve a redirect as a qr image"""
        redirect = Redirect.objects.create(
            target_url_list=['https://google.com/'])
        url_with_qr_param = detail_url(redirect.id) + '?qr'
        self.client.force_authenticate(user=self.admin)
        response = self.client.get(url_with_qr_param)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response._headers['content-type'][1],
            'image/png'
        )
