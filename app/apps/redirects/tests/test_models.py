"""Tests for redirect model."""

from django.test import TestCase

from apps.redirects.models import (
    Redirect, create_redirect_hash, settings, ContentFile)


class RedirectModelMixin(TestCase):

    @staticmethod
    def make_redirect(url='http://google.com', **kwargs):
        redirect = Redirect(target_url_list=[url], **kwargs)
        redirect.save()
        return redirect


class RedirectTestCase(RedirectModelMixin, TestCase):

    def test_create_redirect_with_hash_on_save(self):
        redirect = self.make_redirect()
        self.assertEqual(Redirect.objects.count(), 1)
        self.assertNotEqual(redirect.hash, '')
        self.assertEqual(
            redirect.target_url_list[redirect.default_index],
            'http://google.com'
        )

    def test_different_url_requests_make_unique_redirects(self):
        redirect1 = self.make_redirect('http://google.com')
        redirect2 = self.make_redirect('http://espn.com')
        self.assertNotEqual(redirect1.hash, redirect2.hash)

    def test_create_redirect_random_hash_with_16_characters(self):
        s1 = create_redirect_hash()
        s2 = create_redirect_hash()
        self.assertEqual(len(s1), 16)
        self.assertNotEqual(s1, s2)

    def test_string_representation_is_redirect_link(self):
        redirect = self.make_redirect()
        self.assertEqual(
            settings.REDIRECT_DOMAIN + 'r/' + redirect.hash,
            str(redirect)
        )

    def test_get_default_target_url(self):
        redirect = self.make_redirect('http://test.com')
        self.assertEqual(
            redirect.get_default_target_url(),
            'http://test.com'
        )

    def test_get_redirect_qr(self):
        redirect = self.make_redirect('http://test.com')
        qr = redirect.get_qr()
        self.assertEqual(
            isinstance(qr, ContentFile),
            True
        )
