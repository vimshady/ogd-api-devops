 [![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
 [![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://bitbucket.org/ogd-team/)
 [![forthebadge kill-em](http://ForTheBadge.com/images/badges/oooo-kill-em.svg)](https://www.youtube.com/watch?v=uP6fkha9TVQ)

 # Welcome to the OGD REST API

 This is the back-end API serving redirect links and QR codes. It was originally built on top of source code for a Udemy course "Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/)".

It features the following technologies:

 - Python
 - Django / Django-REST-Framework / Dynamic REST
 - Docker / Docker-Compose
 - Terraform
 - AWS (RDS, S3, ECS, EC2, DynamoDB, ALB, VPC, Subnets, IAM)

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

 ## Additional Background
  ---
 - [Data Model](docs/data-flow.md)
 - [API Specs](docs/api-specs.md)
 - [File Structure](docs/file-structure.md)
 - [Development](docs/development.md)
 - [Testing / Linting](docs/testing.md)
 - [Building / Deploying](docs/dev-ops.md)
 - [Hosting / Serving](docs/hosting.md)
 - [Security](docs/security.md)
 - [Writing Issues](docs/issues.md)
 - [FAQs](docs/faqs.md)
~
